
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\{{$modelname}};

class {{$modelname}}Controller extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        ${{strtolower($modelname)}}s = {{$modelname}}::orderBy('created_at','desc')->get();
        return view('admin.{{strtolower($modelname)}}.list')->with('{{strtolower($modelname)}}',${{strtolower($modelname).'s'}});
    }

    public function store(Request $request)
    { 
        ${{strtolower($modelname)}} = new {{$modelname}};
        @foreach($params as $p)
        
        ${{strtolower($modelname)}}->{{$p['name']}} = $request->{{$p['name']}};
        @endforeach

        ${{strtolower($modelname)}}->save();

        $request->session()->flash('success', 'New {{strtolower($modelname)}} created');

        return redirect()->route('{{strtolower($modelname)}}.index');
    }

  
    public function show($id)
    {
        ${{strtolower($modelname)}} = {{$modelname}}::find($id);
        if(!${{strtolower($modelname)}})
            abort(404);

        return view('admin.{{strtolower($modelname).'s'}}.show')->with('{{strtolower($modelname)}}',${{strtolower($modelname)}});
    }

    
  
    public function update(Request $request, $id)
    {
        ${{strtolower($modelname)}} = {{$modelname}}::find($id);
        if(!${{strtolower($modelname)}}) abort(404);

        @foreach($params as $p)
        ${{strtolower($modelname)}}->{{$p['name']}} = $request->{{$p['name']}};
        @endforeach

        ${{strtolower($modelname)}}->save();

        $request->session()->flash('success', '{{$modelname}} upated');

        return redirect()->route('{{strtolower($modelname)}}.index');
    }

    public function destroy($id,Request $request)
    {
        ${{strtolower($modelname)}} = {{$modelname}}::find($id);
        if(!${{strtolower($modelname)}}) abort(404);

        ${{strtolower($modelname)}}->delete();

        $request->session()->flash('success', '{{$modelname}} deleted');

        return redirect()->route('{{strtolower($modelname)}}.index');
    }
}
