
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{{$catmodelname}};
use App\{{$itemmodelname}};
use Auth;

class {{$itemmodelname}}Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        {{strtolower($itemmodelname).'s'}} = {{$itemmodelname}}::orderBy('id','desc')->get();
        $cat={{$catmodelname}}::all();
    	return view ('downloads.index')->with('{{strtolower($itemmodelname).'s'}}',{{strtolower($itemmodelname).'s'}})->with('cat',$cat);
    }

    public function store(Request $request)
    {
        {{strtolower($itemmodelname)}}=new {{$itemmodelname}};
        @foreach($itemparams as $p)
            {{strtolower($itemmodelname)}}->{{$p['name']}}=$request->{{$p['name']}};
        @endforeach
        {{strtolower($itemmodelname)}}->{{strtolower($catmodelname)}}_id=$request->{{strtolower($catmodelname).'_id'}};
     	{{strtolower($itemmodelname)}}->save();
     	session()->flash('message','{{$itemmodelname}} were successfully added');
     	return redirect()->route('{{strtolower($itemmodelname)}}.index'); 

    }

    public function update(Request $request)
    {
        {{strtolower($itemmodelname)}}={{$itemmodelname}}::find($request->id);
        {{strtolower($itemmodelname)}}->title=$request->edittitle;
        {{strtolower($itemmodelname)}}->category_id=$request->category;
     
        {{strtolower($itemmodelname)}}->save();
        session()->flash('success','{{$itemmodelname}} was Updated successfully!');

        return redirect()->route('{{strtolower($itemmodelname)}}.index'); 

    }

    public function destroy(Request $request)
    {
    	{{strtolower($itemmodelname)}} = {{$itemmodelname}}::findorFail($request->id);
       
         {{strtolower($itemmodelname)}}->delete();
         return redirect()->route('{{strtolower($itemmodelname)}}.index');
    }

    public function storeCat(Request $request)
    {
        $cat=new {{$catmodelname}};
        @foreach($catparams as $p)
        $cat->{{$p['name']}}=$request->{{$p['name']}};
        @endforeach
        $cat->save();
        session()->flash('success','{{$catmodelname}} was added successfully');
        return redirect()->route('{{strtolower($itemmodelname)}}.index');
    }

    public function deletecat(Request $request)
    {
        $cat={{$catmodelname}}::findorFail($request->id);
        $cat->delete();
        return redirect()->route('{{strtolower($itemmodelname)}}.index');
    }

    public function updatecat(Request $request)
    {
        
        
        $cat = {{$catmodelname}}::find($request->get('id'));
        @foreach($catparams as $p)
        $cat->{{$p['name']}}= $request->{{$p['name']}};
        @endforeach
        $cat->save();
        session()->flash('message','{{$catmodelname}} Succesfully Edited');
        return redirect()->route('{{strtolower($itemmodelname)}}.index');
    }
}
