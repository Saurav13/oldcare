
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class {{'Create'.$catmodelname.'sTable'}} extends Migration
{
    public function up()
    {
        Schema::create('{{strtolower($catmodelname).'s'}}', function (Blueprint $table) {

            $table->increments('id');
            
            @foreach($catparams as $p)

        $table->{{$p['type']}}('{{$p['name']}}'{{$p['type']=='enum'?',['.$p['enumValues'].']':''}}){{$p['nullable']?'->nullable()':''}}{{$p['default']?'->default("'.$p['default'].'")':''}}{{$p['unique']?'->unique()':''}};
        {{-- $table=>{{$p->type}}('{{$p->name}}');   --}}
        @endforeach
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists("{{strtolower($catmodelname).'s'}}");
    }
}
