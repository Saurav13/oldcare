var l = angular.module('onetomanyapp',[]);
l.controller('OneToManyController',function($scope,$http){
    $scope.catparams=[];
    $scope.itemparams=[];
    $scope.cataddParams=function(){
        var temp={'name':'' , 'type': '' ,'nullable': false , 'default': '','enumValues':'','unique':false}
        $scope.catparams.push(temp);
    }
    $scope.itemaddParams=function(){
        var temp={'name':'' , 'type': '' ,'nullable': false , 'default': '','enumValues':'','unique':false}
        $scope.itemparams.push(temp);
    }
    $scope.publish=function(){
        $http({ method: "POST", url: "/admin/onetomany/publish", data: {'catparams':$scope.catparams ,'catmodelname':$scope.catmodelname,'itemparams':$scope.itemparams ,'itemmodelname':$scope.itemmodelname }
    }).then(function success(response) {
        alert("success");

		},function error(response){
			alert('wrong');
		});
    }
});