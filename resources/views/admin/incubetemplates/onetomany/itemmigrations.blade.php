
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class {{'Create'.$itemmodelname.'sTable'}} extends Migration
{
    public function up()
    {
        Schema::create('{{strtolower($itemmodelname).'s'}}', function (Blueprint $table) {

            $table->increments('id');
        $table->integer('{{strtolower($catmodelname).'_id'}}')->unsigned();
            @foreach($itemparams as $p)

        $table->{{$p['type']}}('{{$p['name']}}'{{$p['type']=='enum'?',['.$p['enumValues'].']':''}}){{$p['nullable']?'->nullable()':''}}{{$p['default']?'->default("'.$p['default'].'")':''}}{{$p['unique']?'->unique()':''}};
        {{-- $table=>{{$p->type}}('{{$p->name}}');   --}}
        @endforeach
        $table->foreign('{{strtolower($catmodelname).'_id'}}')->references('id')->on('{{strtolower($catmodelname).'s'}}')->onDelete('cascade');
     
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists("{{strtolower($modelname).'s'}}");
    }
}
