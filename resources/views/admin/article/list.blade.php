@extends('layouts.admin')
@section('body')
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">Articles</h2><br>
                <button type="button" class="btn btn-primary btn-min-width " style="float:right;" data-toggle="modal" data-target="#createform">
                        Create
                    </button>
               
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="table-responsive">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                                                <th>title</th>
                                                                <th>description</th>
                                                                <th>reach</th>
                                                                <th>status</th>
                                                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($article as $no)
                            <tr>
                                                                <th scope="row">{{$no->id}}</th>
                                                            <td><text id="articletitle{{$no->id}}">{{$no->title}}</text></td>
                                                             <td><text id="articledescription{{$no->id}}">{{$no->description}}</text></td>
                                                             <td><text id="articlereach{{$no->id}}">{{$no->reach}}</text></td>
                                                             <td><text id="articlestatus{{$no->id}}">{{$no->status}}</text></td>
                                                                 
                                <td>
                                 <button article_id="{{$no->id}}" class="btn btn-info" type="button" data-toggle="modal" id="article_edit{{$no->id}}" data-target="#editform">Edit</button>
                                <form action="/admin/article/{{$no->id}}" method="post">
                                    {{csrf_field()}}
                                    <input name="_method" id ="delete" type="hidden" value="DELETE">
                                    <button class="btn btn-danger"id="delete" type ="submit" value="delete">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                    </div>
               
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- Table head2 options end -->
    <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                
<!-- notes ADDING  FORM Modal 2-->
                            <div class="modal fade text-xs-left" id="createform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Add The Details</label>
                                        </div>
                                        <form class= "Form" action="/admin/article/" method = "POST"  files="true" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="modal-body">
                                                <label>article title: </label>
                                                                                                                                                                    <label for="projectinput8">title</label>
                                                        <div class="form-group">
                                                            <input type="text"  class="form-control" placeholder="Enter title" name="title">
                                                        </div>
                                                                                                                                                                                                                            <div class="form-group">
                                                            <label for="projectinput8">About Project</label>
                                                            <textarea id="projectinput8" rows="5" class="form-control" name="description" placeholder="Enter description"></textarea>
                                                        </div>
                                                                                                                                                                                                                            <label for="projectinput8">reach</label>
                                                        <div class="form-group">
                                                            <input type="number"  class="form-control" placeholder="Enter reach" name="reach">
                                                        </div>
                                                                                                                                                                                                                            <div class="form-group">
                                                            <label for="issueinput6">status</label>
                                                            
                                                            <select id="issueinput6" name="status" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                               
                                                                                                                                <option value="active">active</option>
                                                                                                                                <option value="passive">passive</option>
                                                                                                                            </select>
                                                        </div>
                                                                                                                                                             
                                                <div class="modal-footer">
                                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                                </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>
    </div>
   <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                

                
<!-- notes Modaledit the details 1_4 -->
                            <div class="modal fade text-xs-left" id="editform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit The subjects</label>
                                        </div>
                                        <form class= "Form"  id="editformmodal"action=""method = "post" files="true" enctype="multipart/form-data" >
                                            {{csrf_field()}}
                                           <input type="hidden" name="_method" value="PATCH">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    
								            	<input type="hidden"  id="articleidmodaleditid" value="" class="form-control" placeholder="id" name="id">
                                                </div>
                                    
                                         
                                                                                                                                <div class="form-group">
                                                    <label for="formarticletitleedit">title</label>
                                                <input type="text" id="formarticletitleedit" class="form-control" placeholder="Enter title" name="title">
                                            </div>
                                                                                                                                                                              <div class="form-group">
                                                <label for="formarticledescriptionedit">description</label>
                                                <textarea id="formarticledescriptionedit" rows="5" class="form-control" name="description" placeholder="Enter description"></textarea>
                                            </div>
                                                                                                                                                                              <div class="form-group">
                                                    <label for="formarticlereachedit">reach</label>
                                                <input type="number" id="formarticlereachedit" class="form-control" placeholder="Enter reach" name="reach">
                                            </div>
                                                                                                                                                                              <div class="form-group">
                                                <label for="formarticlestatusedit">status</label>
                                                
                                                <select id="formarticlestatusedit" name="status" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                
                                                                                                        <option value="active">active</option>
                                                                                                        <option value="passive">passive</option>
                                                                                                    </select>
                                            </div>
                                                                                      							                
                                        <div class="modal-footer">
                                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                            <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                        </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>

                                           
    








</div>


@endsection
@section("js")
<script>
        
      $("[id*='delete']").click(function(e){
        var ele = this;
        e.preventDefault();
        
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this record!",
          type: "warning",

          showCancelButton: true,

        }).then(function(){
          ele.form.submit();
        }).catch(swal.noop);

      });
    
$('document').ready(function(){
    $('[id*= "article_edit"]').click(function(){
            var article_id = $(this).attr('article_id');
           // console.log(note_id);
                        var articletitle =$('#articletitle'+article_id).html();
                        var articledescription =$('#articledescription'+article_id).html();
                        var articlereach =$('#articlereach'+article_id).html();
                        var articlestatus =$('#articlestatus'+article_id).html();
                        $('#formarticleidmodaleditid').val(article_id);
                        $('#formarticletitleedit').val(articletitle);
                        $('#formarticledescriptionedit').val(articledescription);
                        $('#formarticlereachedit').val(articlereach);
                        $('#formarticlestatusedit').val(articlestatus);
                        
            $('#editformmodal').attr("action","/admin/article/"+article_id);
           
    });
});
</script>
@endsection