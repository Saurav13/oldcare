@@extends('layouts.admin')
@@section('body')
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h2 class="card-title">{{$modelname.'s'}}</h2><br>
                <button type="button" class="btn btn-primary btn-min-width " style="float:right;" data-toggle="modal" data-target="#createform">
                        Create
                    </button>
               
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="table-responsive">
                     <div class="col-xs-12 col-sm-12 col-md-12">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                @foreach($params as $p)
                                <th>{{$p['name']}}</th>
                                @endforeach
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @@foreach(${{strtolower($modelname)}} as $no)
                            <tr>
                                <?php $a='->' ?>
                                <th scope="row">@{{$no->id}}</th>
                                @foreach($params as $p)
                            <td><text id="{{strtolower($modelname)}}{{$p['name']}}@{{$no->id}}">{{ '{'.'{'.'$'.'no'.$a.$p['name']."}"."}"}}</text></td>
                                 @endforeach
                                
                                <td>
                                 <button {{strtolower($modelname)}}_id="@{{$no->id}}" class="btn btn-info" type="button" data-toggle="modal" id="{{strtolower($modelname)}}_edit@{{$no->id}}" data-target="#editform">Edit</button>
                                <form action="/admin/{{strtolower($modelname)}}/@{{$no->id}}" method="post">
                                    @{{csrf_field()}}
                                    <input name="_method" id ="delete" type="hidden" value="DELETE">
                                    <button class="btn btn-danger"id="delete" type ="submit" value="delete">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @@endforeach
                        </tbody>
                    </table>
                
                    </div>
               
                </div>
            </div>
        </div>
    </div>
</div>
  <!-- Table head2 options end -->
    <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                
<!-- notes ADDING  FORM Modal 2-->
                            <div class="modal fade text-xs-left" id="createform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Add The Details</label>
                                        </div>
                                        <form class= "Form" action="/admin/{{strtolower($modelname)}}/" method = "POST"  files="true" enctype="multipart/form-data">
                                            @{{csrf_field()}}
                                            <div class="modal-body">
                                                <label>{{strtolower($modelname)}} title: </label>
                                                    @foreach($params as $p)
                                                        @if($p['type']=='string')
                                                        <label for="projectinput8">{{$p['name']}}</label>
                                                        <div class="form-group">
                                                            <input type="text"  class="form-control" placeholder="Enter {{strtolower($p['name'])}}" name="{{strtolower($p['name'])}}">
                                                        </div>
                                                        @elseif($p['type']=='text')
                                                        <div class="form-group">
                                                            <label for="projectinput8">About Project</label>
                                                            <textarea id="projectinput8" rows="5" class="form-control" name="{{strtolower($p['name'])}}" placeholder="Enter {{strtolower($p['name'])}}"></textarea>
                                                        </div>
                                                        @elseif($p['type']=='enum')
                                                        <div class="form-group">
                                                            <label for="issueinput6">{{strtolower($p['name'])}}</label>
                                                            
                                                            <select id="issueinput6" name="{{strtolower($p['name'])}}" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                               
                                                                @foreach(explode(',',$p['enumValues']) as $op)
                                                                <option value="{{str_replace("'","",$op)}}">{{str_replace("'","",$op)}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        @elseif($p['type']=='integer')
                                                        <label for="projectinput8">{{$p['name']}}</label>
                                                        <div class="form-group">
                                                            <input type="number"  class="form-control" placeholder="Enter {{strtolower($p['name'])}}" name="{{strtolower($p['name'])}}">
                                                        </div>
                                                        @elseif($p['type']=='boolean')
                                                            <div class="form-group">
                                                                <label for="issueinput6">{{strtolower($p['name'])}}</label>
                                                                
                                                                <select id="issueinput6" name="{{strtolower($p['name'])}}" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                                    
                                                                    <option value="0">false</option>
                                                                    <option value="1">true</option>
                                                                    
                                                                    
                                                                </select>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                 
                                                <div class="modal-footer">
                                                    <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                                    <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                                </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>
    </div>
   <div class="card-body collapse in">
		<div class="card-block">
			<div class="row my-2">
			    <div class="col-lg-4 col-md-6 col-sm-12">
					<div class="form-group">
                
                

                
<!-- notes Modaledit the details 1_4 -->
                            <div class="modal fade text-xs-left" id="editform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <label class="modal-title text-text-bold-600" id="myModalLabel33">Edit The subjects</label>
                                        </div>
                                        <form class= "Form"  id="editformmodal"action=""method = "post" files="true" enctype="multipart/form-data" >
                                            @{{csrf_field()}}
                                           <input type="hidden" name="_method" value="PATCH">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    
								            	<input type="hidden"  id="{{strtolower($modelname)}}idmodaleditid" value="" class="form-control" placeholder="id" name="id">
                                                </div>
                                    
                                         
                                        @foreach($params as $p)
                                            @if($p['type']=='string')
                                            <div class="form-group">
                                                    <label for="form{{strtolower($modelname)}}{{$p['name']}}edit">{{$p['name']}}</label>
                                                <input type="text" id="form{{strtolower($modelname)}}{{$p['name']}}edit" class="form-control" placeholder="Enter {{strtolower($p['name'])}}" name="{{strtolower($p['name'])}}">
                                            </div>
                                            @elseif($p['type']=='text')
                                            <div class="form-group">
                                                <label for="form{{strtolower($modelname)}}{{$p['name']}}edit">{{$p['name']}}</label>
                                                <textarea id="form{{strtolower($modelname)}}{{$p['name']}}edit" rows="5" class="form-control" name="{{strtolower($p['name'])}}" placeholder="Enter {{strtolower($p['name'])}}"></textarea>
                                            </div>
                                            @elseif($p['type']=='enum')
                                            <div class="form-group">
                                                <label for="form{{strtolower($modelname)}}{{$p['name']}}edit">{{strtolower($p['name'])}}</label>
                                                
                                                <select id="form{{strtolower($modelname)}}{{$p['name']}}edit" name="{{$p['name']}}" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                
                                                    @foreach(explode(',',$p['enumValues']) as $op)
                                                    <option value="{{str_replace("'","",$op)}}">{{str_replace("'","",$op)}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @elseif($p['type']=='integer')
                                            <div class="form-group">
                                                    <label for="form{{strtolower($modelname)}}{{$p['name']}}edit">{{$p['name']}}</label>
                                                <input type="number" id="form{{strtolower($modelname)}}{{$p['name']}}edit" class="form-control" placeholder="Enter {{strtolower($p['name'])}}" name="{{strtolower($p['name'])}}">
                                            </div>
                                            @elseif($p['type']=='boolean')
                                            <div class="form-group">
                                                <label for="form{{strtolower($modelname)}}{{$p['name']}}edit">{{strtolower($p['name'])}}</label>
                                                
                                                <select id="form{{strtolower($modelname)}}{{$p['name']}}edit" name="{{$p['name']}}" class="form-control" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Status" data-original-title="" title="">
                                                
                                                   
                                                    <option value="0">false</option>
                                                    <option value="1">true</option>

                                                    
                                                </select>
                                            </div>
                                            @endif
                                          @endforeach
							                
                                        <div class="modal-footer">
                                            <input type="reset" class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="close">
                                            <input type="submit" class="btn btn-outline-primary btn-lg" value="submit">
                                        </div>
                                        </form>		
                                    </div>
                                </div>

                            </div>
                         </div>
                    </div>
                </div>
            </div>
       
        </div>
    </div>

                                           
    








</div>


@@endsection
@@section("js")
<script>
        
      $("[id*='delete']").click(function(e){
        var ele = this;
        e.preventDefault();
        
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this record!",
          type: "warning",

          showCancelButton: true,

        }).then(function(){
          ele.form.submit();
        }).catch(swal.noop);

      });
    
$('document').ready(function(){
    $('[id*= "{{strtolower($modelname)}}_edit"]').click(function(){
            var {{strtolower($modelname)}}_id = $(this).attr('{{strtolower($modelname)}}_id');
           // console.log(note_id);
            @foreach($params as $p)
            var {{strtolower($modelname)}}{{$p['name']}} =$('#{{strtolower($modelname)}}{{$p["name"]}}'+{{strtolower($modelname)}}_id).html();
            @endforeach
            $('#form{{strtolower($modelname)}}idmodaleditid').val({{strtolower($modelname)}}_id);
            @foreach($params as $p)
            $('#form{{strtolower($modelname)}}{{$p['name']}}edit').val({{strtolower($modelname)}}{{$p['name']}});
            @endforeach
            
            $('#editformmodal').attr("action","/admin/{{strtolower($modelname)}}/"+{{strtolower($modelname)}}_id);
           
    });
});
</script>
@@endsection