
namespace App;

use Illuminate\Database\Eloquent\Model;

class {{$catmodelname}} extends Model
{
    protected $table='{{strtolower($catmodelname).'s'}}';
    public function {{strtolower($itemmodelname)}}s()
    {
    	return $this->hasMany({{$itemmodelname}}::class);
    }
}
       