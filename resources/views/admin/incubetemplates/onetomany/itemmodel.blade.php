
namespace App;

use Illuminate\Database\Eloquent\Model;

class {{$itemmodelname}} extends Model
{
    protected $table='{{strtolower($itemmodelname).'s'}}';
   public function {{strtolower($catmodelname)}}()
    {
    	return $this->belongsTo({{$catmodelname}}::class);
    }
}
