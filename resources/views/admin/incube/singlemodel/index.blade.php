@extends('layouts.admin')

@section('body')
@if (count($errors)>0 )
    <div class="alert alert-dismissible fade in mb-2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="color:black">&times;</span>
        </button>
        <ul class="list-group">
            @foreach ($errors->all() as $error)
            <li class="list-group-item list-group-item-danger">{{ $error }}</li>
            @endforeach
        </ul>
    </div>	
@endif
<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">Incube</h2>
    </div>
</div>
<div class="content-body" ng-app="modelapp" ng-controller="ModelController">
    <div class="card" style="height: 943px;">
        <div class="card-header">
            <h4 class="card-title" id="basic-layout-form">Model Info</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form class="form" ng-submit="publish()" method="POST">
                    <div class="form-body">
                        <h4 class="form-section"><i class="icon-head"></i>Model</h4>
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="modelname">Name</label>
                                    <input type="text" id="modelname" ng-model="modelname" class="form-control" placeholder="First Name" name="modelname">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput3">E-mail</label>
                                    <input type="text" id="projectinput3" class="form-control" placeholder="E-mail" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput4">Contact Number</label>
                                    <input type="text" id="projectinput4" class="form-control" placeholder="Phone" name="phone">
                                </div>
                            </div>
                        </div> --}}

                        <h4 class="form-section"><i class="icon-clipboard4"></i> Parameters</h4>
                        <a href="#!" class="btn btn-success" ng-click="addParams()">Add param</a>
                        <div class="table-responsive" >
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Paramenter</th>
                                        <th>Type</th>
                                        <th>Nullable</th>
                                        <th>Default</th>
                                        <th>Unique</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="p in params">
                                        <td><input type="text" ng-model="p.name" placeholder="Name of param"/></td>
                                        <td>
                                            <select ng-model="p.type">
                                                <option disabled selected>Select Type</option>
                                                <option value="integer">integer</option>
                                                <option value="text">text</option>
                                                <option value="string">string</option>
                                                <option value="boolean">boolean</option>
                                                <option value="enum">enum</option>
                                            </select>
                                            <input type="text" placeholder="enum values" ng-model="p.enumValues" ng-show="p.type=='enum'"/>
                                        </td>    
                                        <td>
                                            <input type="checkbox" ng-model="p.nullable"/>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Default value" ng-model="p.default"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" ng-model="p.unique"/>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput5">Interested in</label>
                                    <select id="projectinput5" name="interested" class="form-control">
                                        <option value="none" selected="" disabled="">Interested in</option>
                                        <option value="design">design</option>
                                        <option value="development">development</option>
                                        <option value="illustration">illustration</option>
                                        <option value="branding">branding</option>
                                        <option value="video">video</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput6">Budget</label>
                                    <select id="projectinput6" name="budget" class="form-control">
                                        <option value="0" selected="" disabled="">Budget</option>
                                        <option value="less than 5000$">less than 5000$</option>
                                        <option value="5000$ - 10000$">5000$ - 10000$</option>
                                        <option value="10000$ - 20000$">10000$ - 20000$</option>
                                        <option value="more than 20000$">more than 20000$</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label>Select File</label>
                            <label id="projectinput7" class="file center-block">
                                <input type="file" id="file">
                                <span class="file-custom"></span>
                            </label>
                        </div> --}}

                        {{-- <div class="form-group">
                            <label for="projectinput8">About Project</label>
                            <textarea id="projectinput8" rows="5" class="form-control" name="comment" placeholder="About Project"></textarea>
                        </div> --}}
                    </div>

                    <div class="form-actions">
                      
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> Publish
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



   @endsection

   @section('js')
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
<script src="{{asset('admin-assets/modelapp.js')}}"></script>
   @endsection