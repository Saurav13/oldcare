@extends('layouts.admin')

@section('body')
@if (count($errors)>0 )
    <div class="alert alert-dismissible fade in mb-2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" style="color:black">&times;</span>
        </button>
        <ul class="list-group">
            @foreach ($errors->all() as $error)
            <li class="list-group-item list-group-item-danger">{{ $error }}</li>
            @endforeach
        </ul>
    </div>	
@endif
<div class="content-header row">
    <div class="content-header-left col-md-6 col-xs-12 mb-1">
        <h2 class="content-header-title">One to Many</h2>
    </div>
</div>
<div class="content-body" ng-app="onetomanyapp" ng-controller="OneToManyController">
    <div class="card" style="height: 943px;">
        <div class="card-header">
            <h4 class="card-title" id="basic-layout-form">Model Info</h4>
            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form class="form" ng-submit="publish()" method="POST">
                    <div class="form-body">
                        <h4 class="form-section"><i class="icon-head"></i>Category Model</h4>
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="catmodelname">Name</label>
                                    <input type="text" id="catmodelname" ng-model="catmodelname" class="form-control" placeholder="First Name" name="catmodelname">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput3">E-mail</label>
                                    <input type="text" id="projectinput3" class="form-control" placeholder="E-mail" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projectinput4">Contact Number</label>
                                    <input type="text" id="projectinput4" class="form-control" placeholder="Phone" name="phone">
                                </div>
                            </div>
                        </div> --}}

                        <h4 class="form-section"><i class="icon-clipboard4"></i> Parameters</h4>
                        <a href="#!" class="btn btn-success" ng-click="cataddParams()">Add param</a>
                        <div class="table-responsive" >
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Paramenter</th>
                                        <th>Type</th>
                                        <th>Nullable</th>
                                        <th>Default</th>
                                        <th>Unique</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="p in catparams">
                                        <td><input type="text" ng-model="p.name" placeholder="Name of param"/></td>
                                        <td>
                                            <select ng-model="p.type">
                                                <option disabled selected>Select Type</option>
                                                <option value="integer">integer</option>
                                                <option value="text">text</option>
                                                <option value="string">string</option>
                                                <option value="boolean">boolean</option>
                                                <option value="enum">enum</option>
                                            </select>
                                            <input type="text" placeholder="enum values" ng-model="p.enumValues" ng-show="p.type=='enum'"/>
                                        </td>    
                                        <td>
                                            <input type="checkbox" ng-model="p.nullable"/>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Default value" ng-model="p.default"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" ng-model="p.unique"/>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-body">
                        <h4 class="form-section"><i class="icon-head"></i>Item Model</h4>
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="itemmodelname">Name</label>
                                    <input type="text" id="itemmodelname" ng-model="itemmodelname" class="form-control" placeholder="First Name" name="itemmodelname">
                                </div>
                            </div>
                        </div>
                       
                        <h4 class="form-section"><i class="icon-clipboard4"></i> Parameters</h4>
                        <a href="#!" class="btn btn-success" ng-click="itemaddParams()">Add param</a>
                        <div class="table-responsive" >
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Paramenter</th>
                                        <th>Type</th>
                                        <th>Nullable</th>
                                        <th>Default</th>
                                        <th>Unique</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="p in itemparams">
                                        <td><input type="text" ng-model="p.name" placeholder="Name of param"/></td>
                                        <td>
                                            <select ng-model="p.type">
                                                <option disabled selected>Select Type</option>
                                                <option value="integer">integer</option>
                                                <option value="text">text</option>
                                                <option value="string">string</option>
                                                <option value="boolean">boolean</option>
                                                <option value="enum">enum</option>
                                            </select>
                                            <input type="text" placeholder="enum values" ng-model="p.enumValues" ng-show="p.type=='enum'"/>
                                        </td>    
                                        <td>
                                            <input type="checkbox" ng-model="p.nullable"/>
                                        </td>
                                        <td>
                                            <input type="text" placeholder="Default value" ng-model="p.default"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" ng-model="p.unique"/>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                        
                    </div>

                    <div class="form-actions">
                      
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> Publish
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



   @endsection

   @section('js')
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
<script src="{{asset('admin-assets/onetomanyapp.js')}}"></script>
   @endsection