
<script>
        
        $("[id*='delete']").click(function(e){
          var ele = this;
          e.preventDefault();
          
          swal({
            title: "Are you sure?",
            text: "You will not be able to recover this record!",
            type: "warning",
  
            showCancelButton: true,
  
          }).then(function(){
            ele.form.submit();
          }).catch(swal.noop);
  
        });
      
  $('document').ready(function(){
      $('[id*= "note_edit"]').click(function(){
              var note_id = $(this).attr('note_id');
             // console.log(note_id);
              var notetitle =$('#note_title'+note_id).html();
              var notesubjects =$('#notesubjects'+note_id).html();
             // console.log(notetitle);
             // consle.log(notesubjects);
              $('#noteidmodaleditid').val(note_id);
              $('#formnotetitleedit').val(notetitle);
              $("#notesubjecteditmodal").val(notesubjects);
              $('#notesformmodal').attr("action","/admin/note/"+note_id);
             
      });
  });
  </script>